<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Response;

use App\User;
use App\Repositories\User\UserRepository;

class UserController extends Controller
{   

    public function index()
    {

        try 
        {

            $data = UserRepository::listar();
            $status = '200';
            $message = "Registros listados com sucesso!";
            $success = 'true';
            return ResponseBuilder::result($success,$status,$message,$data);
    
        }
        catch (\Exception $e) 
        {
            
            \Log::error(
                $e->getMessage(),
                [
                    'linha' => $e->getLine(),
                    'arquivo' => $e->getFile(),
                    'trace' => $e->getTrace()
                ]
            );
            
            return ResponseBuilder::result('false',$e->getCode(),$e->getMessage());

        }

    }
    


    public function show(Request $request, $id)
    {

        try 
        {

            $data = UserRepository::listarPorId($request, $id);
            $status = '200';
            $message = "Registros listados com sucesso!";
            $success = 'true';
            return ResponseBuilder::result($success,$status,$message,$data);
    
        }
        catch (\Exception $e) 
        {
            
            \Log::error(
                $e->getMessage(),
                [
                    'linha' => $e->getLine(),
                    'arquivo' => $e->getFile(),
                    'trace' => $e->getTrace()
                ]
            );
            
            return ResponseBuilder::result('false',$e->getCode(),$e->getMessage());

        }

    }



    public function store(Request $request)
    {

        try 
        {

            $data = UserRepository::incluir($request);
            $status = '200';
            $message = "Registros listados com sucesso!";
            $success = 'true';
            return ResponseBuilder::result($success,$status,$message,$data);
    
        }
        catch (\Exception $e) 
        {
            
            \Log::error(
                $e->getMessage(),
                [
                    'linha' => $e->getLine(),
                    'arquivo' => $e->getFile(),
                    'trace' => $e->getTrace()
                ]
            );
            
            return ResponseBuilder::result('false',$e->getCode(),$e->getMessage());

        }

    }



    public function update(Request $request, $id)
    {
        
        try 
        {

            $data = UserRepository::atualizar($request, $id);
            $status = '200';
            $message = "Registros listados com sucesso!";
            $success = 'true';
            return ResponseBuilder::result($success,$status,$message,$data);
    
        }
        catch (\Exception $e) 
        {
            
            \Log::error(
                $e->getMessage(),
                [
                    'linha' => $e->getLine(),
                    'arquivo' => $e->getFile(),
                    'trace' => $e->getTrace()
                ]
            );
            
            return ResponseBuilder::result('false',$e->getCode(),$e->getMessage());

        }

    }



    public function destroy($id)
    {

        try 
        {

            $data = UserRepository::excluir($id);
            $status = '200';
            $message = "Registros listados com sucesso!";
            $success = 'true';
            return ResponseBuilder::result($success,$status,$message,$data);
    
        }
        catch (\Exception $e) 
        {
            
            \Log::error(
                $e->getMessage(),
                [
                    'linha' => $e->getLine(),
                    'arquivo' => $e->getFile(),
                    'trace' => $e->getTrace()
                ]
            );
            
            return ResponseBuilder::result('false',$e->getCode(),$e->getMessage());

        }

    }

}
