<?php

namespace App\Repositories\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Repositories\User\UserInterface as UserInterface;

class UserRepository implements UserInterface
{

    
    public static function listar()
    {

        $users = User::all();
        return $users;

    }



    public static function listarPorId(Request $request, $id)
    {

        
    }



    public static function incluir(Request $request)
    {

        
    }



    public static function atualizar(Request $request, $id)
    {

        

    }



    public static  function excluir($id)
    {

        

    }

}
