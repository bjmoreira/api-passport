<?php

namespace App\Repositories\User;

use Illuminate\Http\Request;

interface UserInterface
{
    public static function listar();
    
    public static function listarPorId(Request $request, $id);

    public static function incluir(Request $request);
    
    public static function atualizar(Request $request, $id);

    public static function excluir($id);

}